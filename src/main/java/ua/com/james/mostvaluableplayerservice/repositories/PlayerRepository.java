package ua.com.james.mostvaluableplayerservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.james.mostvaluableplayerservice.models.*;

import java.util.Optional;

public interface PlayerRepository extends JpaRepository<Player, Long> {

    Optional<Player> findByNickname(final String nickname);

}
