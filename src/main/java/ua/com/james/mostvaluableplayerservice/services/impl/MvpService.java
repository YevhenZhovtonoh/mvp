package ua.com.james.mostvaluableplayerservice.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.com.james.mostvaluableplayerservice.exceptions.GameNotSupportedException;
import ua.com.james.mostvaluableplayerservice.exceptions.MvpReadingFileException;
import ua.com.james.mostvaluableplayerservice.models.Player;
import ua.com.james.mostvaluableplayerservice.repositories.PlayerRepository;
import ua.com.james.mostvaluableplayerservice.services.CsvParser;
import ua.com.james.mostvaluableplayerservice.services.PlayerService;

import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;

@Slf4j
@Service
@RequiredArgsConstructor
public class MvpService {

    private final CsvParser csvParser;
    private final List<PlayerService> playerServices;
    private final PlayerRepository playerRepository;

    @Transactional(rollbackOn = Throwable.class)
    public void processFiles(final MultipartFile[] multipartFile, final String game) {
        final Class<?> clazz = GameRecognizer.get(game);
        final List<?> collect = Stream.of(multipartFile).map(file -> processFile(file, clazz))
                .flatMap(Collection::stream).collect(Collectors.toList());
        final PlayerService playerService = getPlayerService(clazz);
        final List<Player> list = playerService.countService(collect);
        list.forEach(this::saveOrUpdate);

    }

    @Transactional(Transactional.TxType.REQUIRED)
    void saveOrUpdate(final Player player) {
        Optional<Player> optionalPlayer = playerRepository.findByNickname(player.getNickname());
        if (optionalPlayer.isPresent()) {
            final Player p = optionalPlayer.get();
            p.setPoints(p.getPoints() + player.getPoints());
            playerRepository.save(p);
        }else {
            playerRepository.save(player);
        }
    }

    // TODO Test
    private List<?> processFile(final MultipartFile multipartFile, final Class<?> clazz) {
        try (InputStream inputStream = multipartFile.getInputStream()) {
            return csvParser.parse(inputStream, clazz);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new MvpReadingFileException("Reading File Exception");
        }
    }

    // TODO Test
    @SuppressWarnings("unchecked")
    private PlayerService getPlayerService(final Class<?> clazz) {
        return playerServices.stream().filter(playerService -> playerService.supports(clazz))
                .findAny().orElseThrow(() -> new GameNotSupportedException("Game not supported"));
    }

    public List<Player> getPlayerRating(){
        List<Player> players = playerRepository.findAll();
        players.sort(comparing(Player::getPoints).reversed());
        return players;
    }

}
