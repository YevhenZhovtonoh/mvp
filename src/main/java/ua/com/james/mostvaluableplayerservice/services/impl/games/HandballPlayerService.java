package ua.com.james.mostvaluableplayerservice.services.impl.games;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import ua.com.james.mostvaluableplayerservice.dto.Basketball;
import ua.com.james.mostvaluableplayerservice.dto.Handball;
import ua.com.james.mostvaluableplayerservice.models.*;
import ua.com.james.mostvaluableplayerservice.services.PlayerService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class HandballPlayerService extends PlayerServiceAbstract<Handball> implements PlayerService<Handball> {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(Handball.class);
    }

    public int process(final Handball handball) {
        return handball.getScoredPoints() * 2 - handball.getGoalsReceived();
    }

}
