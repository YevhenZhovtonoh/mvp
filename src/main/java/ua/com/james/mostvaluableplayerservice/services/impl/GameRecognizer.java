package ua.com.james.mostvaluableplayerservice.services.impl;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ua.com.james.mostvaluableplayerservice.dto.Basketball;
import ua.com.james.mostvaluableplayerservice.dto.Handball;
import ua.com.james.mostvaluableplayerservice.exceptions.GameNotSupportedException;

@Service
public class GameRecognizer {

    public final static String BASKETBALL = "basketball";
    public final static String HANDBALL = "handball";


    public static Class<?> get(final String game) {
        if (game.equals(BASKETBALL)) {
            return Basketball.class;
        }
        if (game.equals(HANDBALL)) {
            return Handball.class;
        }
        throw new GameNotSupportedException("Game is not supported");
    }

}
