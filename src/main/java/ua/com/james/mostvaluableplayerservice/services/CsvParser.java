package ua.com.james.mostvaluableplayerservice.services;

import java.io.InputStream;
import java.util.List;

public interface CsvParser {

    <T> List<T> parse(final InputStream inputStream, final Class<T> tClass);

}