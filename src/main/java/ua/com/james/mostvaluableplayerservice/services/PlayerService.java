package ua.com.james.mostvaluableplayerservice.services;

import ua.com.james.mostvaluableplayerservice.dto.Game;
import ua.com.james.mostvaluableplayerservice.models.Player;

import java.util.List;

public interface PlayerService<T extends Game> {

    List<Player> countService(final List<T> collectionOfGames);

    boolean supports(final Class<?> tClass);

}
