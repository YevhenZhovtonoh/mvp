package ua.com.james.mostvaluableplayerservice.services.impl;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ua.com.james.mostvaluableplayerservice.exceptions.MvpProcessingException;
import ua.com.james.mostvaluableplayerservice.services.CsvParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

@Slf4j
@Service
public class CsvParserImpl implements CsvParser {


    private static final Character SEPARATOR = ';';

    @Override
    public <T> List<T> parse(final InputStream inputStream, final Class<T> clazz) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            final CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(bufferedReader)
                    .withSeparator(SEPARATOR)
                    .withType(clazz)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            return csvToBean.parse();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new MvpProcessingException("Processing of csv failed");
        }
    }
}