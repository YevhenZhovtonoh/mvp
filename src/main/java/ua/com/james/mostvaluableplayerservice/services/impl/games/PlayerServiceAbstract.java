package ua.com.james.mostvaluableplayerservice.services.impl.games;

import org.apache.commons.lang3.tuple.Pair;
import ua.com.james.mostvaluableplayerservice.dto.Game;
import ua.com.james.mostvaluableplayerservice.models.Player;
import ua.com.james.mostvaluableplayerservice.services.PlayerService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract  class PlayerServiceAbstract<T extends  Game> implements PlayerService<T> {

    public abstract int process(final T game);

    public abstract boolean supports(Class<?> clazz);

    protected Player addWinnerPoints(final Pair<Player, T> pair, final String winner) {
        final T basketball = pair.getRight();
        final Player player = pair.getLeft();
        if (basketball.getTeamName().equals(winner)) {
            player.setPoints(player.getPoints() + 10);
        }
        return player;
    }

    protected Pair<Player, T> processPlayer(final T game) {
        final Player player = new Player();
        player.setNickname(game.getNickname());
        player.setPoints(process(game));
        return Pair.of(player, game);
    }

    public List<Player> countService(List<T> playerStatistic) {
        final String winner = getMatchWinner(playerStatistic);
        return playerStatistic.stream()
                .map(this::processPlayer)
                .map(pair -> addWinnerPoints(pair, winner)).collect(Collectors.toList());

    }

    protected String getMatchWinner(List<? extends Game> matchStatistic) {
        final Map<String, Integer> teamAndScore =
                matchStatistic.stream()
                        .collect(Collectors.groupingBy(Game::getTeamName,
                                Collectors.summingInt(Game::getScoredPoints))
                        );

        if (teamAndScore.size() != 2) {
            throw new RuntimeException("Wrong team number in file");
        }

        return Collections.max(teamAndScore.entrySet(),
                Comparator.comparing(Map.Entry::getValue)).getKey();

    }


}
