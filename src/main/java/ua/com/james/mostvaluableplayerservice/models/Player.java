package ua.com.james.mostvaluableplayerservice.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Player implements Comparable<Player> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String nickname;
    private Integer points;

    @Override
    public int compareTo(Player p) {
        return this.getPoints().compareTo(p.getPoints());
    }
}
