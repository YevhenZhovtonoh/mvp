package ua.com.james.mostvaluableplayerservice.dto;

import lombok.Data;

@Data
public class ErrorDto {
    private final String message;
}
