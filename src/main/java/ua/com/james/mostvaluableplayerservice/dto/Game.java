package ua.com.james.mostvaluableplayerservice.dto;

import lombok.Data;

@Data
public abstract class Game {

    protected String nickname;

    protected int scoredPoints;

    protected String teamName;
}
