package ua.com.james.mostvaluableplayerservice.dto;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

@Data
public class Handball extends Game {

    @CsvBindByPosition(position = 0)
    private String name;

    @CsvBindByPosition(position = 1)
    private String nickname;

    @CsvBindByPosition(position = 2)
    private String playerNumber;

    @CsvBindByPosition(position = 3)
    private String teamName;

    @CsvBindByPosition(position = 4)
    private int scoredPoints;

    @CsvBindByPosition(position = 5)
    private int goalsReceived;

}
