package ua.com.james.mostvaluableplayerservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping("/games")
public interface MvpApi {

    @PostMapping("/upload/{game}")
    ResponseEntity uploadFile(@RequestParam(name = "files") final MultipartFile[] multipartFile, final @PathVariable String game) throws IOException;

    @GetMapping("/getrating")
    ResponseEntity getRating();
}
