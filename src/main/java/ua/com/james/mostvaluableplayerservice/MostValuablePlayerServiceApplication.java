package ua.com.james.mostvaluableplayerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MostValuablePlayerServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(MostValuablePlayerServiceApplication.class, args);
	}

}