package ua.com.james.mostvaluableplayerservice.exceptions;

public class MvpProcessingException extends RuntimeException {

    public MvpProcessingException(String message) {
        super(message);
    }

}