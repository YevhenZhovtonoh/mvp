package ua.com.james.mostvaluableplayerservice.exceptions;

public class MvpReadingFileException extends RuntimeException {

    public MvpReadingFileException(String message) {
        super(message);
    }

}