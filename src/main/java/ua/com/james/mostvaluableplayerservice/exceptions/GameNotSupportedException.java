package ua.com.james.mostvaluableplayerservice.exceptions;

public class GameNotSupportedException extends RuntimeException {
    public GameNotSupportedException(String message) {
        super(message);
    }
}
