package ua.com.james.mostvaluableplayerservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ua.com.james.mostvaluableplayerservice.dto.ErrorDto;
import ua.com.james.mostvaluableplayerservice.exceptions.GameNotSupportedException;
import ua.com.james.mostvaluableplayerservice.exceptions.MvpProcessingException;
import ua.com.james.mostvaluableplayerservice.exceptions.MvpReadingFileException;

@RestControllerAdvice
public class MvpGlobalExceptionHandler {

    @ExceptionHandler(MvpProcessingException.class)
    public ResponseEntity<ErrorDto> mvpProcessingException(final MvpProcessingException e) {
        final ErrorDto errorDto = new ErrorDto(e.getMessage());
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorDto);
    }

    @ExceptionHandler(MvpReadingFileException.class)
    public ResponseEntity<ErrorDto> mvpReadingFileException(final MvpReadingFileException e) {
        final ErrorDto errorDto = new ErrorDto(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(GameNotSupportedException.class)
    public ResponseEntity<ErrorDto> gameNotSupportedException(final GameNotSupportedException e) {
        final ErrorDto errorDto = new ErrorDto(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }


}