package ua.com.james.mostvaluableplayerservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ua.com.james.mostvaluableplayerservice.api.MvpApi;
import ua.com.james.mostvaluableplayerservice.services.impl.MvpService;

@RestController
@RequiredArgsConstructor
public class MvpController implements MvpApi {

    private final MvpService mvpService;

    @Override
    public ResponseEntity uploadFile(final MultipartFile[] multipartFile, final String game) {
        mvpService.processFiles(multipartFile, game);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity getRating(){
        return ResponseEntity.ok().body(mvpService.getPlayerRating());
    }

}
