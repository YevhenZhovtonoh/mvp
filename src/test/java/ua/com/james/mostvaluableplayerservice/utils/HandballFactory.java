package ua.com.james.mostvaluableplayerservice.utils;

import ua.com.james.mostvaluableplayerservice.dto.Handball;

public class HandballFactory {

    public static Handball createHandball() {
        Handball handball = new Handball();
        handball.setName("player 1");
        handball.setNickname("nick1");
        handball.setPlayerNumber("4");
        handball.setTeamName("Team A");
        handball.setScoredPoints(0);
        handball.setGoalsReceived(20);
        return handball;
    }

    public static Handball createHandball2() {
        Handball handball = new Handball();
        handball.setName("player 2");
        handball.setNickname("nick2");
        handball.setPlayerNumber("8");
        handball.setTeamName("Team A");
        handball.setScoredPoints(15);
        handball.setGoalsReceived(20);
        return handball;
    }

    public static Handball createHandball3() {
        Handball handball = new Handball();
        handball.setName("player 3");
        handball.setNickname("nick3");
        handball.setPlayerNumber("15");
        handball.setTeamName("Team A");
        handball.setScoredPoints(10);
        handball.setGoalsReceived(20);
        return handball;
    }

    public static Handball createHandball4() {
        Handball handball = new Handball();
        handball.setName("player 4");
        handball.setNickname("nick4");
        handball.setPlayerNumber("16");
        handball.setTeamName("Team B");
        handball.setScoredPoints(1);
        handball.setGoalsReceived(25);
        return handball;
    }

    public static Handball createHandball5() {
        Handball handball = new Handball();
        handball.setName("player 5");
        handball.setNickname("nick5");
        handball.setPlayerNumber("23");
        handball.setTeamName("Team B");
        handball.setScoredPoints(12);
        handball.setGoalsReceived(25);
        return handball;
    }

    public static Handball createHandball6() {
        Handball handball = new Handball();
        handball.setName("player 6");
        handball.setNickname("nick6");
        handball.setPlayerNumber("42");
        handball.setTeamName("Team B");
        handball.setScoredPoints(8);
        handball.setGoalsReceived(25);
        return handball;
    }
}
