package ua.com.james.mostvaluableplayerservice.utils;

import ua.com.james.mostvaluableplayerservice.models.Player;

public class PlayerFactory {
    public static Player createPlayer1() {
        Player player = new Player();
        player.setId(1L);
        player.setPoints(200);
        player.setNickname("Barry");
        return player;
    }

    public static Player createPlayer2() {
        Player player = new Player();
        player.setPoints(480);
        return player;
    }

    public static Player createPlayer3() {
        Player player = new Player();
        player.setId(2L);
        player.setPoints(150);
        player.setNickname("Jimmy");
        return player;
    }

    public static Player createPlayer4() {
        Player player = new Player();
        player.setPoints(355);
        return player;
    }
}
