package ua.com.james.mostvaluableplayerservice.utils;

import ua.com.james.mostvaluableplayerservice.dto.Basketball;

public class BasketballFactory {
    private static final String TEAM_NAME = "Chicago";
    private static final int SCORED_POINTS = 200;
    private static final int REBOUNDS = 20;
    private static final int ASSISTS = 60;

    private static final String TEAM_NAME2 = "New York";
    private static final int SCORED_POINTS2 = 150;
    private static final int REBOUNDS2 = 15;
    private static final int ASSISTS2 = 40;

    public static Basketball createBasketball1() {
        Basketball basketball = new Basketball();
        basketball.setName("player 1");
        basketball.setNickname("nick1");
        basketball.setPlayerNumber("4");
        basketball.setTeamName("Team A");
        basketball.setScoredPoints(10);
        basketball.setRebounds(2);
        basketball.setAssists(7);
        return basketball;
    }

    public static Basketball createBasketball2() {
        Basketball basketball = new Basketball();
        basketball.setName("player 2");
        basketball.setNickname("nick2");
        basketball.setPlayerNumber("8");
        basketball.setTeamName("Team A");
        basketball.setScoredPoints(0);
        basketball.setRebounds(10);
        basketball.setAssists(0);
        return basketball;
    }

    public static Basketball createBasketball3() {
        Basketball basketball = new Basketball();
        basketball.setName("player 3");
        basketball.setNickname("nick3");
        basketball.setPlayerNumber("15");
        basketball.setTeamName("Team A");
        basketball.setScoredPoints(15);
        basketball.setRebounds(10);
        basketball.setAssists(4);
        return basketball;
    }

    public static Basketball createBasketball4() {
        Basketball basketball = new Basketball();
        basketball.setName("player 4");
        basketball.setNickname("nick4");
        basketball.setPlayerNumber("16");
        basketball.setTeamName("Team B");
        basketball.setScoredPoints(20);
        basketball.setRebounds(0);
        basketball.setAssists(0);
        return basketball;
    }

    public static Basketball createBasketball5() {
        Basketball basketball = new Basketball();
        basketball.setName("player 5");
        basketball.setNickname("nick5");
        basketball.setPlayerNumber("23");
        basketball.setTeamName("Team B");
        basketball.setScoredPoints(4);
        basketball.setRebounds(7);
        basketball.setAssists(7);
        return basketball;
    }

    public static Basketball createBasketball6() {
        Basketball basketball = new Basketball();
        basketball.setName("player 6");
        basketball.setNickname("nick6");
        basketball.setPlayerNumber("42");
        basketball.setTeamName("Team B");
        basketball.setScoredPoints(8);
        basketball.setRebounds(10);
        basketball.setAssists(0);
        return basketball;
    }

    public static Basketball createBasketball7() {
        Basketball basketball = new Basketball();
        basketball.setScoredPoints(SCORED_POINTS);
        basketball.setRebounds(REBOUNDS);
        basketball.setAssists(ASSISTS);
        basketball.setTeamName(TEAM_NAME);
        return basketball;
    }

    public static Basketball createBasketball8() {
        Basketball basketball = new Basketball();
        basketball.setScoredPoints(SCORED_POINTS2);
        basketball.setRebounds(REBOUNDS2);
        basketball.setAssists(ASSISTS2);
        basketball.setTeamName(TEAM_NAME2);
        return basketball;
    }
}
