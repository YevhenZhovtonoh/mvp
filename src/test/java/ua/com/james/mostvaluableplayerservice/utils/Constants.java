package ua.com.james.mostvaluableplayerservice.utils;

public interface Constants {
    String PATH_TO_HANDBALL_FILE = "src/test/resources/handball/handball";
    String PATH_TO_BASKETBALL_FILE = "src/test/resources/basketball/basketball1";
    String PLAYER_NICK_NAME = "Barry";
    String WINNER = "Chicago";
    String BASKETBALL = "basketball";
    String HANDBALL = "handball";
    String OTHER_VALUE = "other_value";
    int QUANTITY_PLAYERS = 3;
}
