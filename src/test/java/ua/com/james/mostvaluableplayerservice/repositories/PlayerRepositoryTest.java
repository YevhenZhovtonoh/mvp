package ua.com.james.mostvaluableplayerservice.repositories;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.com.james.mostvaluableplayerservice.models.Player;
import ua.com.james.mostvaluableplayerservice.utils.Constants;
import ua.com.james.mostvaluableplayerservice.utils.PlayerFactory;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PlayerRepositoryTest {

    @Autowired
    private PlayerRepository playerRepository;

    private Player player;

    @Before
    public void setUp() {
        player = PlayerFactory.createPlayer1();
    }

    @Test
    public void findByNickname() {
        playerRepository.save(player);
        assertEquals(Optional.of(player), playerRepository.findByNickname(Constants.PLAYER_NICK_NAME));
    }

    @After
    public void cleanAll() {
        playerRepository.delete(player);
    }
}
