package ua.com.james.mostvaluableplayerservice.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;
import ua.com.james.mostvaluableplayerservice.exceptions.GameNotSupportedException;
import ua.com.james.mostvaluableplayerservice.exceptions.MvpProcessingException;
import ua.com.james.mostvaluableplayerservice.exceptions.MvpReadingFileException;
import ua.com.james.mostvaluableplayerservice.services.impl.MvpService;
import ua.com.james.mostvaluableplayerservice.utils.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
public class MvpGlobalExceptionHandlerTest {
    @InjectMocks
    private MvpController mvpController;
    @Mock
    private MvpService mvpService;

    private MockMvc mockMvc;
    private MultipartFile multipartFile;

    @Before
    public void setUp() throws IOException {
        this.mockMvc = standaloneSetup(mvpController)
                .setMessageConverters(new MappingJackson2HttpMessageConverter())
                .setControllerAdvice(new MvpGlobalExceptionHandler())
                .build();
        InputStream fileInputStream = new FileInputStream(new File(Constants.PATH_TO_HANDBALL_FILE));
        multipartFile = new MockMultipartFile("handball", fileInputStream);
    }

    @Test
    public void mvpProcessingException() throws Exception {
        doThrow(MvpProcessingException.class).when(mvpService)
                .processFiles(any(), any());
        this.mockMvc.perform(post("/games/upload/" + Constants.HANDBALL)
                .requestAttr("files", multipartFile)
                .contentType("multipart/*")
                .content("application/json"))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void mvpReadingFileException() throws Exception {
        doThrow(MvpReadingFileException.class).when(mvpService)
                .processFiles(any(), any());
        this.mockMvc.perform(post("/games/upload/" + Constants.HANDBALL)
                .requestAttr("files", multipartFile)
                .contentType("multipart/*")
                .content("application/json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void gameNotSupportedException() throws Exception {
        doThrow(GameNotSupportedException.class).when(mvpService)
                .processFiles(any(), any());
        this.mockMvc.perform(post("/games/upload/" + Constants.HANDBALL)
                .requestAttr("files", multipartFile)
                .contentType("multipart/*")
                .content("application/json"))
                .andExpect(status().isBadRequest());
    }
}
