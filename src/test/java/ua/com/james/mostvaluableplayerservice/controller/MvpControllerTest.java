package ua.com.james.mostvaluableplayerservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.multipart.MultipartFile;
import ua.com.james.mostvaluableplayerservice.models.Player;
import ua.com.james.mostvaluableplayerservice.services.impl.MvpService;
import ua.com.james.mostvaluableplayerservice.utils.Constants;
import ua.com.james.mostvaluableplayerservice.utils.PlayerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
public class MvpControllerTest {
    @InjectMocks
    private MvpController mvpController;
    @InjectMocks
    private ObjectMapper mapper;
    @Mock
    private MvpService mvpService;

    private MockMvc mockMvc;
    private List<Player> players;

    @Before
    public void setUp() {
        this.mockMvc = standaloneSetup(mvpController)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        players = new ArrayList<>();

        Player player1 = PlayerFactory.createPlayer1();
        Player player2 = PlayerFactory.createPlayer2();
        Player player3 = PlayerFactory.createPlayer3();

        players.add(player1);
        players.add(player2);
        players.add(player3);
    }

    @Test
    public void uploadFile() throws Exception {
        InputStream fileInputStream = new FileInputStream(new File(Constants.PATH_TO_HANDBALL_FILE));
        MultipartFile multipartFile = new MockMultipartFile("handball", fileInputStream);
        MultipartFile[] multipartFiles = {multipartFile};
        doNothing().when(mvpService).processFiles(multipartFiles, Constants.HANDBALL);
        this.mockMvc.perform(post("/games/upload/" + Constants.HANDBALL)
                .requestAttr("files", multipartFile)
                .contentType("multipart/*")
                .content("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void getRating() throws Exception {
        when(mvpService.getPlayerRating()).thenReturn(players);

        MvcResult result = this.mockMvc.perform(get("/games/getrating")
                .contentType("application/json")
                .content("application/json"))
                .andExpect(status().isOk())
                .andReturn();
        String contentResponse = result.getResponse().getContentAsString();
        String playersJsonString = mapper.writeValueAsString(players);
        assertEquals(playersJsonString, contentResponse);
    }
}
