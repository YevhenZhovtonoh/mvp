package ua.com.james.mostvaluableplayerservice.services.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.com.james.mostvaluableplayerservice.dto.Handball;
import ua.com.james.mostvaluableplayerservice.models.Player;
import ua.com.james.mostvaluableplayerservice.repositories.PlayerRepository;
import ua.com.james.mostvaluableplayerservice.services.CsvParser;
import ua.com.james.mostvaluableplayerservice.services.PlayerService;
import ua.com.james.mostvaluableplayerservice.services.impl.games.HandballPlayerService;
import ua.com.james.mostvaluableplayerservice.utils.Constants;
import ua.com.james.mostvaluableplayerservice.utils.HandballFactory;
import ua.com.james.mostvaluableplayerservice.utils.PlayerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MvpServiceTest {

    @Mock
    private PlayerRepository playerRepository;

    @Mock
    private CsvParser csvParser;
    @Mock
    private List<PlayerService> playerServices;
    @Mock
    private HandballPlayerService handballPlayerService;

    @InjectMocks
    private MvpService mvpService;

    private List<PlayerService> playerServicesStub;
    private List<Player> players;
    private List<Player> orderedPlayersByPoints;
    private List<Handball> handballsAllTeamsPlayers;
    private Player player1;

    @Before
    public void setUp() {
        playerServicesStub = new ArrayList<>();
        playerServicesStub.add(new HandballPlayerService());

        players = new ArrayList<>();
        orderedPlayersByPoints = new ArrayList<>();

        player1 = PlayerFactory.createPlayer1();
        Player player2 = PlayerFactory.createPlayer2();
        Player player3 = PlayerFactory.createPlayer3();

        players.add(player1);
        players.add(player2);
        players.add(player3);

        orderedPlayersByPoints.add(player2);
        orderedPlayersByPoints.add(player1);
        orderedPlayersByPoints.add(player3);

        handballsAllTeamsPlayers = new ArrayList<>();
        handballsAllTeamsPlayers.add(HandballFactory.createHandball());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball2());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball3());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball4());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball5());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball6());
    }

    @Test
    public void processFiles() throws Exception {
//        InputStream inputStreamHandball = new FileInputStream(new File(Constants.PATH_TO_HANDBALL_FILE));
//        MultipartFile multipartFile = new MockMultipartFile(Constants.HANDBALL, inputStreamHandball);
//        MultipartFile[] multipartFiles = {multipartFile};
//        when(csvParser.parse(any(), any())).thenReturn(Collections.singletonList(handballsAllTeamsPlayers));
//        when(playerServices.stream()).thenReturn(playerServicesStub.stream());
//        mvpService.processFiles(multipartFiles, Constants.HANDBALL);
    }

    @Test
    public void saveOrUpdate() {
        when(playerRepository.findByNickname(Constants.PLAYER_NICK_NAME)).thenReturn(Optional.of(player1));
        when(playerRepository.save(any(Player.class))).thenReturn(player1);
        mvpService.saveOrUpdate(player1);
        verify(playerRepository, times(1)).save(any(Player.class));
    }

    @Test
    public void saveOrUpdateIfNotFoundInDBTest() {
        when(playerRepository.findByNickname(Constants.PLAYER_NICK_NAME)).thenReturn(Optional.empty());
        when(playerRepository.save(any(Player.class))).thenReturn(player1);
        mvpService.saveOrUpdate(player1);
        verify(playerRepository, times(1)).save(any(Player.class));
    }

    @Test
    public void getPlayerRating() {
        when(playerRepository.findAll()).thenReturn(players);
        List<Player> playersList = mvpService.getPlayerRating();
        assertThat(Constants.QUANTITY_PLAYERS, equalTo(playersList.size()));
        assertThat(Constants.QUANTITY_PLAYERS, not(playersList.size() + 1));
        assertThat(orderedPlayersByPoints, equalTo(playersList));
    }
}
