package ua.com.james.mostvaluableplayerservice.services.impl;

import org.junit.Test;
import ua.com.james.mostvaluableplayerservice.dto.Basketball;
import ua.com.james.mostvaluableplayerservice.dto.Handball;
import ua.com.james.mostvaluableplayerservice.exceptions.GameNotSupportedException;
import ua.com.james.mostvaluableplayerservice.services.impl.GameRecognizer;
import ua.com.james.mostvaluableplayerservice.utils.Constants;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

public class GameRecognizerTest {

    @Test
    public void getBasketballTest() {
        Class basketballClass = GameRecognizer.get(Constants.BASKETBALL);
        assertThat(basketballClass, equalTo(Basketball.class));
        assertThat(basketballClass, not(Handball.class));
    }

    @Test
    public void getHandballTest() {
        Class handballClass = GameRecognizer.get(Constants.HANDBALL);
        assertThat(handballClass, equalTo(Handball.class));
        assertThat(handballClass, not(Basketball.class));
    }

    @Test(expected = GameNotSupportedException.class)
    public void getGameNotSupportedExceptionTest() {
        GameRecognizer.get(Constants.OTHER_VALUE);
    }
}
