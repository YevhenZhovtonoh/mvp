package ua.com.james.mostvaluableplayerservice.services.impl.games;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import ua.com.james.mostvaluableplayerservice.dto.Basketball;
import ua.com.james.mostvaluableplayerservice.models.Player;
import ua.com.james.mostvaluableplayerservice.utils.BasketballFactory;
import ua.com.james.mostvaluableplayerservice.utils.Constants;
import ua.com.james.mostvaluableplayerservice.utils.PlayerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceAbstractTest {

    private PlayerServiceAbstract playerServiceAbstract;
    private Basketball basketball;
    private Player player;
    private Player player2;
    private List<Basketball> matchStatistic;
    private List<Player> winnersTeam;

    @Before
    public void setUp() {
        playerServiceAbstract = new BasketballPlayerService();
        basketball = BasketballFactory.createBasketball7();
        player = PlayerFactory.createPlayer1();
        player2 = PlayerFactory.createPlayer2();
        Player player4 = PlayerFactory.createPlayer4();
        winnersTeam = new ArrayList<>();
        winnersTeam.add(player2);
        winnersTeam.add(player4);
        matchStatistic = new ArrayList<>();
        matchStatistic.add(basketball);
        matchStatistic.add(BasketballFactory.createBasketball8());
    }

    @Test
    public void addWinnerPoints() {
        Pair<Player, Basketball> pair = Pair.of(player, basketball);
        assertEquals(Integer.valueOf(basketball.getScoredPoints() + 10), playerServiceAbstract.addWinnerPoints(pair, Constants.WINNER).getPoints());
    }

    @Test
    public void processPlayer() {
        Pair<Player, Basketball> pair = Pair.of(player2, basketball);
        assertEquals(pair, playerServiceAbstract.processPlayer(basketball));
    }

    @Test
    public void countService() {
        winnersTeam.get(0).setPoints(winnersTeam.get(0).getPoints() + 10);
        assertEquals(winnersTeam, playerServiceAbstract.countService(matchStatistic));
    }

    @Test
    public void getMatchWinner() {
        assertTrue(matchStatistic.get(0).getScoredPoints() > matchStatistic.get(1).getScoredPoints());
        assertFalse(matchStatistic.get(0).getScoredPoints() < matchStatistic.get(1).getScoredPoints());
        assertEquals(Constants.WINNER, matchStatistic.get(0).getTeamName());
        assertEquals(Constants.WINNER, playerServiceAbstract.getMatchWinner(matchStatistic));
    }
}
