package ua.com.james.mostvaluableplayerservice.services.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import ua.com.james.mostvaluableplayerservice.dto.Basketball;
import ua.com.james.mostvaluableplayerservice.dto.Handball;
import ua.com.james.mostvaluableplayerservice.exceptions.MvpProcessingException;
import ua.com.james.mostvaluableplayerservice.services.impl.CsvParserImpl;
import ua.com.james.mostvaluableplayerservice.utils.BasketballFactory;
import ua.com.james.mostvaluableplayerservice.utils.Constants;
import ua.com.james.mostvaluableplayerservice.utils.HandballFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CsvParserImplTest {

    @InjectMocks
    private CsvParserImpl csvParser;
    private List<Handball> handballsAllTeamsPlayers;
    private List<Basketball> basketballAllTeamsPlayers;

    @Before
    public void setUp() {
        handballsAllTeamsPlayers = new ArrayList<>();
        handballsAllTeamsPlayers.add(HandballFactory.createHandball());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball2());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball3());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball4());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball5());
        handballsAllTeamsPlayers.add(HandballFactory.createHandball6());

        basketballAllTeamsPlayers = new ArrayList<>();
        basketballAllTeamsPlayers.add(BasketballFactory.createBasketball1());
        basketballAllTeamsPlayers.add(BasketballFactory.createBasketball2());
        basketballAllTeamsPlayers.add(BasketballFactory.createBasketball3());
        basketballAllTeamsPlayers.add(BasketballFactory.createBasketball4());
        basketballAllTeamsPlayers.add(BasketballFactory.createBasketball5());
        basketballAllTeamsPlayers.add(BasketballFactory.createBasketball6());
    }

    @Test
    public void parse() throws FileNotFoundException {
        InputStream inputStreamHandball = new FileInputStream(new File(Constants.PATH_TO_HANDBALL_FILE));
        InputStream inputStreamBasketball = new FileInputStream(new File(Constants.PATH_TO_BASKETBALL_FILE));
        List<Handball> handballList = csvParser.parse(inputStreamHandball, Handball.class);
        List<Basketball> basketballList = csvParser.parse(inputStreamBasketball, Basketball.class);
        assertEquals(handballsAllTeamsPlayers, handballList);
        assertEquals(basketballAllTeamsPlayers, basketballList);
    }

}
