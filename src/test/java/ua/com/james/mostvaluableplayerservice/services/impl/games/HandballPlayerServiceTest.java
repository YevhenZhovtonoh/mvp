package ua.com.james.mostvaluableplayerservice.services.impl.games;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import ua.com.james.mostvaluableplayerservice.dto.Basketball;
import ua.com.james.mostvaluableplayerservice.dto.Handball;
import ua.com.james.mostvaluableplayerservice.services.impl.games.HandballPlayerService;
import ua.com.james.mostvaluableplayerservice.utils.HandballFactory;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class HandballPlayerServiceTest {

    @InjectMocks
    private HandballPlayerService handballPlayerService;

    private Handball handball;
    private static final int PROCESS_VALUE = -20;

    @Before
    public void setUp() {
        handball = HandballFactory.createHandball();
    }

    @Test
    public void supportsTest() {
        assertTrue(handballPlayerService.supports(Handball.class));
    }

    @Test
    public void notSupportsTest() {
        assertFalse(handballPlayerService.supports(Basketball.class));
    }

    @Test
    public void processTest() {
        assertEquals(PROCESS_VALUE, handballPlayerService.process(handball));
    }
}
