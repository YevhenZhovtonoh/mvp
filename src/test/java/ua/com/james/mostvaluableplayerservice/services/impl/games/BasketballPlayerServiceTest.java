package ua.com.james.mostvaluableplayerservice.services.impl.games;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import ua.com.james.mostvaluableplayerservice.dto.Basketball;
import ua.com.james.mostvaluableplayerservice.dto.Handball;
import ua.com.james.mostvaluableplayerservice.utils.BasketballFactory;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BasketballPlayerServiceTest {

    @InjectMocks
    private BasketballPlayerService basketballPlayerService;

    private Basketball basketball;

    private static final int PROCESS_VALUE = 29;


    @Before
    public void setUp() {
        basketball = BasketballFactory.createBasketball1();
    }

    @Test
    public void supportsTest() {
        assertTrue(basketballPlayerService.supports(Basketball.class));
    }

    @Test
    public void notSupportsTest() {
        assertFalse(basketballPlayerService.supports(Handball.class));
    }

    @Test
    public void processTest() {
        assertEquals(PROCESS_VALUE, basketballPlayerService.process(basketball));
    }
}
